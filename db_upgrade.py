#!venv/bin/python
''' Mod Doc String'''
from migrate.versioning import api
import app
import os.path
from app import CONFIG_CACHE


SQLALCHEMY_DATABASE_URI = CONFIG_CACHE['SQLALCHEMY_DATABASE_URI']
SQLALCHEMY_MIGRATE_REPO = CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO']

api.upgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
V = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
print('Current Database Version: ' + str(V))
