The Joe Page
======

## About:
This is nothing more than my personal website. 

### Features:

+ Implemented:
    + Password Protected Flask Admin - The Flask Admin ist accessible at `/admin` which redirects to `/login` if you did not log in yet

    + Custom CMS You can add blog posts and skills, which show up on their corresponding page

    + John Joessons Bootstrap4Builder - Customize Bootstrap and compile it togehter with the bootstrap base utilizing gulp ( `nodejs must be installed` )
    
    + Password Hashing
    
+ Not Implemented Yet:
    + SSL
    


## Installation Instructions:

As an example, it will work best, if you create a Python 3 virtual environment.

You will reveice this package without the env folder, so first you need to create it.
Make sure u use the latest python 3. Find out by executing 
`python -v`

If this outputs something abouth python 2.7, do the same with python3. If this is
the was your OS has python 3 installed, u will need to use that instead.

`python -m venv venv` or `pythin3 -m venv venv`



#### activate the env:

`. venv/bin/activate` ( i alias this command on my machines to `ave` and strongly recommend to you doing the same)


#### install python dependencies


`pip install -r reqs.txt`

or if your os aliases python 3.x as python3:

`pip3 install -r reqs.txt`

#### configure app

see config.py for some configuration possibilites. you could also create your own config file, which will override the default file. make sure to set every config the default file sets in there.

I recommend leaving the migration repository setting as it is. This will create the migration repository inside the app folder or sets the app to use the one provided with this repo.

#### (optional) utilize John Joessons Bootstrap4Builder

this package comes with installed and compiled bootstrap, popper and jquery. if you want to customize it, you will need `nodejs` and `gulp`. 

you can then run ` npm install` and `gulp`in the app/static/ directory.
if you only need stock bootstrap, there should be no need to do anything.

#### Create DB

You will need to create a Database for this application to work. 
This projects provides the scripts for that. As I learned Flask with Miguel Grinbergs excellent tutorial, I used the scripts he wrote, with minimal changes ( as to determine which config file shuold be used and so on).

Inside the root directory you find the following files:

    db_create.py
    db_migrate.py
    db_upgrade.py
    db_create_user.py

Now execute:
```bash
./db_create.py
./db_upgrade.py
```

This will set up the database and create all tables. 

next, run `./db_create_user.py`, enter a Name and Password.
The password will be hashed and written into the database. Editing
it will break the users log in. Also, adding a user with any sql browser
wont let you be able to log in.

#### Finally: Start the app

run `./run.py` from the command line and then visit localhost:5000 in your browser.

access the admin route to log in. you will be redirected to the admin page, where you can logout.


## Deployment Instructions
on lcodes.de you can see this site in action. I will update this readme with 
the steps I took to configure nginx and gunicorn on ubuntu to serve the site. 
