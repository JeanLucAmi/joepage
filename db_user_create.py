#!venv/bin/python
from app import app, db, CONFIG_CACHE, models
import os.path
from passlib.hash import sha256_crypt
from datetime import date
import getpass



print('This script creates a new user for you. Password will be hashed.')
print('Please enter a username')

name = input()

pw_valid = False

while not pw_valid:
    print('Please enter a password')
    pw1 = getpass.getpass()
    print('Please enter your password again')
    pw2 = getpass.getpass()
    if pw1 == pw2:
        pw_valid = True
        print("Creating new user %s" % name)
    else:
        print("Passwords did not match.\nplease try again")


pw_hash = sha256_crypt.encrypt(pw1)

try:
    user = models.User()
    user.name = name
    user.password = pw_hash
    user.created_at = date.today()
    user.verify_password(pw1)
    db.session.add(user)
    db.session.commit()
except Exception as error:
    print(error)

print("All Done")


