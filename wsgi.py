from app import app
from app import CONFIG_CACHE


def main():
    ssl = False
    if 'SSL' in CONFIG_CACHE:
        if CONFIG_CACHE['SSL']:
            print('establishing https')
            ssl = True
        else:
            print('establishing http')
    if ssl:
        app.run(debug=True, ssl_context='adhoc')
    else:
        app.run(debug=True)


if __name__ == '__main__':
    main()