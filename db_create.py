#!venv/bin/python
from migrate.versioning import api
from app import app, db
import os.path
from app import CONFIG_CACHE
 
db.create_all()
if not os.path.exists(CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO']):
    api.create(CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO'], 'database repository')
    api.version_control(CONFIG_CACHE['SQLALCHEMY_DATABASE_URI'], CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO'])
else:
    api.version_control(CONFIG_CACHE['SQLALCHEMY_DATABASE_URI'], CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO'], api.version(CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO']))