from app import db
from app import UserMixin
from passlib.hash import sha256_crypt


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(255))
    created_at = db.Column(db.DateTime())
    last_login = db.Column(db.DateTime())
    

    def __repr__(self):
        return '<user %r>' % (self.name)

    def verify_password(self, data):

        return sha256_crypt.verify(data, self.password)



class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True, )
    title = db.Column(db.String(40))
    body = db.Column(db.String())
    created_at = db.Column(db.DateTime())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    tags = db.Column(db.String())

    user = db.relationship('User', foreign_keys=user_id)

    def get_date_string(self):
        ''' querys date from db and replaces dash with dot '''
        return (str(self.created_at).split()[0].replace('-', '.'))

    def __repr__(self):
        return '<Title %r , Id %i >' % (self.title, self.id)


class Link(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140))
    body = db.Column(db.String(140))
    url = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    tags = db.Column(db.String())

    def __repr__(self):
        return '<Link %r %r>' % (self.body, self.url)


class Skill(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    category = db.Column(db.String(140))
    isSubCategory = db.Column(db.Boolean())
    bullets = db.Column(db.String())
    order = db.Column(db.Integer())
    
    def get_bullet_list(self):
        return self.bullets.split(',')
        


    def __repr__(self):
        return '<Category %r with %r | Is Sub? %r >' % (self.category, self.bullets, self.isSubCategory) 





'''
Skills
Programmiersprachen}
Getypte Sprachen}
.NET C\#, Java
Schwach getypte Sprachen}
Python, JS, PHP
Markup / Textsatz}
{\LaTeX}, HTML, CSS, Markdown, XAML
Frameworks / Engines}
Flask ( Python ), Unity Engine ( C\# ), Wordpress ( Plugins mit PHP )
Betriebssysteme}
Desktop:}
Windows 7 - 10, Linux ( Arch Linux und Debian basierte Systeme)
Server:}
Windows Server 2008 R2 ( Exchange 2008, Active Directory, Terminalserver ), CentOs, Ubuntu Server
Datenbanken}
SQL} Postgresql, SQLite, MS SQL

Tools}
IDE : }
Visual Studio, Eclipse, Pycharm
Texteditoren: }
Visual Studio Code, Atom
Versionskontrolle: }
Git
CLI: }
Bash, zsh, etwas Windows Powershell
Virtualisierung}
VMWare VSphere 5.5, Virtualbox

Fremdsprachen}
English: } Gut in Wort und Schrift

'''