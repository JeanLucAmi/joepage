from flask import Flask, flash, session, wrappers, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_wtf import Form
from flask_admin.contrib.sqla import ModelView

from flask_login import LoginManager, UserMixin
from functools import wraps

# ===== Section Config ===== #
import os

CONFIG_CACHE = {}
app = Flask(__name__)
conf_path = os.path.join(app.root_path, 'config_user.py') 
print(conf_path)
if os.path.isfile(conf_path):
    print("User Config detected... using that.")
    app.config.from_pyfile('config_user.py')
    CONFIG_CACHE = app.config.copy()

else:
    print("NO User Config detected... using default config...")
    app.config.from_pyfile('config.py')
    CONFIG_CACHE = app.config.copy()

# ===== Section Connect DB===== #
db = SQLAlchemy(app)
# ===== Section LOGIN Manager ===== #
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if session and session.get('logged_in'):
            return f(*args, **kwargs)
        else:
            flash("Login in Please")
            return redirect(url_for('login'))

    return wrap

# ===== App init ===== #
from app import views, models, forms, custom_admin, dbhelpers

# ===== Section Initialize Admin Page ===== #
admin = Admin(
            app, 
            name="joepage", 
            template_mode='bootstrap3',
            index_view=custom_admin.MyView())


admin.add_view(ModelView(models.User, db.session))
admin.add_view(ModelView(models.Link, db.session))
admin.add_view(custom_admin.BlogPostModelView(models.Post, db.session))
admin.add_view(ModelView(models.Skill, db.session))


