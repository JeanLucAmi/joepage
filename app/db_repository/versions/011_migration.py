from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
category = Table('category', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=20)),
)

link_category = Table('link_category', post_meta,
    Column('category_id', Integer, primary_key=True, nullable=False),
    Column('link_id', Integer, primary_key=True, nullable=False),
)

post = Table('post', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('title', String),
    Column('created_at', DateTime),
    Column('author', String),
)

post_category = Table('post_category', post_meta,
    Column('category_id', Integer, primary_key=True, nullable=False),
    Column('post_id', Integer, primary_key=True, nullable=False),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['category'].create()
    post_meta.tables['link_category'].create()
    post_meta.tables['post'].create()
    post_meta.tables['post_category'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['category'].drop()
    post_meta.tables['link_category'].drop()
    post_meta.tables['post'].drop()
    post_meta.tables['post_category'].drop()
