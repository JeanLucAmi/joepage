from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
role = Table('role', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('name', VARCHAR(length=80)),
    Column('description', VARCHAR(length=255)),
)

roles_users = Table('roles_users', pre_meta,
    Column('user_id', INTEGER),
    Column('role_id', INTEGER),
)

user = Table('user', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('active', BOOLEAN),
    Column('confirmed_at', TIMESTAMP),
    Column('email', VARCHAR(length=255)),
    Column('first_name', VARCHAR(length=255)),
    Column('last_name', VARCHAR(length=255)),
    Column('password', VARCHAR(length=255)),
)

user = Table('user', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=80)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['role'].drop()
    pre_meta.tables['roles_users'].drop()
    pre_meta.tables['user'].columns['active'].drop()
    pre_meta.tables['user'].columns['confirmed_at'].drop()
    pre_meta.tables['user'].columns['email'].drop()
    pre_meta.tables['user'].columns['first_name'].drop()
    pre_meta.tables['user'].columns['last_name'].drop()
    pre_meta.tables['user'].columns['password'].drop()
    post_meta.tables['user'].columns['name'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['role'].create()
    pre_meta.tables['roles_users'].create()
    pre_meta.tables['user'].columns['active'].create()
    pre_meta.tables['user'].columns['confirmed_at'].create()
    pre_meta.tables['user'].columns['email'].create()
    pre_meta.tables['user'].columns['first_name'].create()
    pre_meta.tables['user'].columns['last_name'].create()
    pre_meta.tables['user'].columns['password'].create()
    post_meta.tables['user'].columns['name'].drop()
