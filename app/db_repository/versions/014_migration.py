from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
link_category = Table('link_category', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('category_id', INTEGER),
    Column('link_id', INTEGER),
)

post_categories = Table('post_categories', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('category_id', INTEGER),
    Column('post_id', INTEGER),
)

post = Table('post', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('title', String(length=40)),
    Column('body', String),
    Column('created_at', DateTime),
    Column('user_id', Integer),
    Column('tags', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['link_category'].drop()
    pre_meta.tables['post_categories'].drop()
    post_meta.tables['post'].columns['tags'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['link_category'].create()
    pre_meta.tables['post_categories'].create()
    post_meta.tables['post'].columns['tags'].drop()
