var gulp = require('gulp');

gulp.task('default', function () {
    var sass         = require('gulp-sass');
    var postcss      = require('gulp-postcss');
    var sourcemaps   = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer');
    var cssnano      = require('cssnano');

    console.log('+++ Building Bootstrap4 (John Joesson)');

    return gulp.src('./bootstrap.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([
          autoprefixer({ browsers: ['last 2 versions'] }),
          cssnano()
          ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./build'));
});