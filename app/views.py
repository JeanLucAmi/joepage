'''
Views
=====
'''
from flask import render_template, flash, redirect, url_for, request, session

from app import app, models, forms, dbhelpers


# ==== Section Routes ==== #
@app.route('/')
@app.route('/index')
def index():
    '''renders index page'''
    active = "home"
    title = active.upper()
    return render_template(
        'index.html',
        active=active,
        title=title)


@app.route('/blogentry', defaults={'entry': 1})
@app.route('/blogentry/<int:entry>')
def blogentry(entry=1):
    blogpost = dbhelpers.get_post_by_id(entry)
    active = 'blog'
    title = active.upper()
    overview = dbhelpers.get_post_list()

    return render_template(
        'blogentry.html',
        title = title,
        active = active,
        blogpost = blogpost,
        overview = overview
        )



@app.route('/blog', defaults={'page': 1})
@app.route('/blog/<int:page>')
def blog(page=1):
    '''fstring'''
    per_page = 4
    posts = dbhelpers.get_posts_pagination(per_page, page)

    active = 'blog'
    title = active.upper()
    return render_template(
        'blog.html',
        title=title,
        active=active,
        posts=posts,

    )

@app.route('/about')
def method_name():
    active = 'about'
    title = active.upper()
    skills = dbhelpers.get_skill_list()
    return render_template('about.html', 
        skills=skills,
        active=active,
        title=title)

@app.route('/login', methods=['GET', 'POST'])
def login():

    form = forms.LoginForm()
    if form.validate_on_submit():
        attempted_username = form.username.data
        attempted_pw = form.password.data

        user = models.User.query.filter_by(name=attempted_username).first()

        if user is not None and user.verify_password(attempted_pw):
            session['logged_in'] = True
            session['username'] = attempted_username
            flash('login success, Hi there ' + session.get('username'))
            return redirect(url_for('admin.index', _external=True))
        else:
            flash('Fuck you!')

    return render_template("login.html", title='login', form=form)
