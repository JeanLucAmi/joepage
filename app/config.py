'''
Config File for this Project. You should leave it as it is, if you want to customize you should create a new File in this directory
and name it "config_user.py".
'''
import os
basedir = os.path.abspath(os.path.dirname(__file__))


# ===== ssl
SSL = False

# ===== Config for WTF:
# Cross Site Request Forgery protection for WTForms
WTF_CSRF_ENABLED = True 
SECRET_KEY = 'very_secret'

# ===== DB Config
# path to your db, uses a simple sqllight file inside project directory if you not set this.
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db') 
 # path to your migration repo. leave this as default if you like it inside the app.
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')