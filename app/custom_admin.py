from flask_admin import AdminIndexView, expose, BaseView
from flask_admin.contrib.sqla import ModelView
from wtforms import TextAreaField
from wtforms.widgets import TextArea

class CKTextAreaWidget(TextArea):
    def __call__(self, field, **kwargs):
        if kwargs.get('class'):
            kwargs['class'] += ' ckeditor'
        else:
            kwargs.setdefault('class', 'ckeditor')
        return super(CKTextAreaWidget, self).__call__(field, **kwargs)

from app import login_required, session, redirect, url_for, flash

class CKTextAreaField(TextAreaField):
    widget = CKTextAreaWidget()

class MyView(AdminIndexView):

    # @login_required
    @expose('/')
    def index(self):
        if session.get('logged_in'):
            return super(MyView, self).index()
        else:
            return redirect(url_for('login'))

    @expose('/logout')
    @login_required
    def logout(self):
        session.clear()
        return redirect(url_for('index'))



class BlogPostModelView(ModelView):
    extra_js = ['//cdn.ckeditor.com/4.6.0/standard/ckeditor.js']

    form_overrides = {
        'body':CKTextAreaField        
    }
