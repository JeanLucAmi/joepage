'''
Some Helper Functions and DB queries
'''
from app import models

def get_skill_list():
    ''' returns all skills as list'''
    return models.Skill.query.all()

def get_post_list():
    ''' Returns all posts as list '''
    return models.Post.query.all()

def get_post_by_id(post_id):
    '''Returns any post by id'''
    return models.Post.query.filter_by(id=post_id).first()

def get_posts_pagination(per_page, page):
    ''' return pagination element for posts'''
    return models.Post.query.order_by(
        models.Post.created_at.desc()).paginate(
            page,
            per_page,
            error_out=False)


def get_links_by_categorie():
    ''' deprecated and without use '''
    return None
    ''' linkcats = models.Link.query.all()
    entries = []

    for entrie in linkcats:
        cat_id = int(entrie.tags)
        category = models.Category.query.filter_by(id=cat_id).first()
        name = str.lower(category.name)

        belongs_to_cat = (name == cat)
        
        if(belongs_to_cat):

            link = models.Link.query.filter_by(id=entrie.link_id).first()
            entries.append(link)
    print('entries = ' + str(entries))
    return entries '''
